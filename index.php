<?php include "includes/header.php" ?>

<?php 
$range;
$coating;
$lightning;
$coatingMessage = "Coating Damage";
$lightningMessage = "Lightning Strike";

if(isset($_POST['range'])){
    $range = $_POST['range'];
    $coating = $_POST['coatingMultiple'];
    $lightning = $_POST['lightningMultiple'];
}
?>


    <div class="row">
        <div class="col-md-12">
            <h2 class="page-header text-center">Tower Fizz</h2>
        </div>
    </div>
    <section id="login">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-6 col-xs-offset-3">
                <div class="form-wrap">
                    <form role="form" action="index.php" method="post" id="login-form" autocomplete="off">
                        <div class="form-group">
                            <label for="range" class="">Range 0 - ?:</label>
                            <input type="number" name="range" id="range" min="0" max="999" value="99"  class="form-control" placeholder="Enter top of range, 0-?">
                        </div>
                        <div class="form-group">
                            <label for="coatingMultiple" class="">Multiple for Coating Message</label>
                            <input type="number" name="coatingMultiple" id="coatingMultiple" min="1" value="<?php echo $coating ?>" class="form-control" placeholder="Multiple For 'Coating' Message">
                        </div>
                        <div class="form-group ">
                            <label for="lightningMultiple" class="">Multiple for Lightning Message</label>
                            <input type="number" name="lightningMultiple" min="1" value="<?php echo $lightning ?>" id="lightningMultiple" class="form-control" placeholder="Multiple For 'Lightning' Message">
                        </div>
                        <input type="submit" name="submit" id="btn-login" class="btn btn-warning btn-lg btn-block" value="Show Report">
                    </form>
                 
                </div>
            </div>
        </div>
    </div> 
</section>

<table class="table">
  <thead>
    <tr>
      <th scope="col">Counter</th>
      <th scope="col">Output</th>
    </tr>
  </thead>
  <tbody>

<?php 
if(isset($_POST['range']) && isset($_POST['coatingMultiple'])){
$counter = 0;
foreach(range(0, $range) as $number){
    echo '<tr>';
    echo '<th scope="row">'.$counter.'</th>';
    if($number % $coating == 0 && $number % $lightning == 0){
        echo '<td>'.$coatingMessage. ' and '.$lightningMessage. '</td>';
    } 
    elseif($number % $coating == 0){
        echo '<td>'.$coatingMessage, '</td>';
    }
    elseif($number % $lightning == 0){
        echo '<td>'.$lightningMessage, '</td>';
    }
    else{
        echo '<td>'.$number, '</td>';
    }

  echo'</tr>';
  $counter++;
    }
}
?>
  </tbody>
</table>


<?php include "includes/footer.php" ?>